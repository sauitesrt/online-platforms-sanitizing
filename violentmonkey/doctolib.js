// ==UserScript==
// @name         Doctolib
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.doctolib.fr/*
// @grant        none
// @require http://code.jquery.com/jquery-3.4.1.min.js
// ==/UserScript==

(function () {
  'use strict';

  setInterval(() => {
    $('.availabilities-substition-name')
      .closest('.dl-search-result')
      .css('background-color', '#f2c4ca');
  }, 200);
})();
