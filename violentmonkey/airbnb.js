// ==UserScript==
// @name         Airbnb
// @namespace    Violentmonkey Scripts
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.airbnb.com/*
// @grant        none
// ==/UserScript==

(async function () {
  'use strict';

  let minReviews = 20;
  let minRating1 = 4.5;
  let minRating2 = 4.8;

  while (true) {
    await updateListings();
    await new Promise((resolve) => setTimeout(resolve, 300));
  }

  async function updateListings() {
    let listings = Array.from(document.querySelectorAll('._8ssblpx')).map(
      (el) => {
        let res = el.querySelector('[aria-label*="Rating"]');
        if (!res) {
          return {
            el,
            res: null,
          };
        }
        res = res
          .getAttribute('aria-label')
          .match(/Rating (\d\.\d+) out of 5; (\d+) reviews/);
        return {
          el,
          res: {
            rating: Number.parseFloat(res[1]),
            reviews: Number.parseInt(res[2]),
          },
        };
      }
    );

    let setBg = (el, color) => {
      el.style.backgroundColor = color;
    };

    for (let { el, res } of listings) {
      const green = '#b3ffb3';
      const orange = '#ffb84d';
      const red = '#ff8080';

      if (!res) {
        setBg(el, red);
        continue;
      }
      let { rating, reviews } = res;
      console.log(rating, reviews);

      if (reviews < minReviews && rating < 4.9) {
        setBg(el, orange);
      } else if (rating < minRating1) {
        setBg(el, red);
      } else if (rating < minRating2) {
        setBg(el, orange);
      } else {
        setBg(el, green);
      }
    }
  }
})();
