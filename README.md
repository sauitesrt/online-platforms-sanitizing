This is a dump of my ublock-origin filters / violentmonkey scripts that help me sanitize online platforms on the browser.

The underlying principle is that I want to spend as little time on online platforms as possible so that I can quickly do what I wanted to do on that particular platform and move on.

- Youtube
  - Suppression of the feed and video ratings
- Reddit
  - Suppression of the likes counts
- Linkedin
  - Suppression of the feed
- Facebook
  - Suppression of the feed
- Instagram
  - Suppression of the feed and likes counts
- Amazon
  - Suppression of purchase suggestions.
  - Suppression of the ratings in the search results which slow down my decision-making heuristics (overload of information).
  - Suppression of various annoyances.
  - TODO : remove any other information in the search results, except the pictures, titles and prices but I'm too lazy to do it right now.
- Airbnb
  - Coloring of the listings in the search results (green, orange, red) to help my decision-making heuristics. I find ratings on Airbnb useful as in my experience they are rather accurate because they are from verified customers. I'm sure that some listings without top ratings are great too, but I don't find that it's worth the hassle to take any risk on that platform.
- Doctolib (French platfrom to make a booking with a health professional)
  - Marking of practicians in the search result who are not available but have someone to replace them. By experience it's better to only deal with the regular practician.
